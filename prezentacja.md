---
author: Jakub Kinast
title: Najpiekniejsze miejsca na Ziemi
subtitle:
date: 07.11.2020
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}

---

## Slajd nr 1 - Wielka Rafa Koralowa

Wpisana na listę światowego dziedzictwa przyrody UNESCO **Wielka Rafa Koralowa** rozciąga się na przestrzeni około 345 km kwadratowych wzdłuż australijskiego wybrzeża, w odległości od 15 do 200 km od brzegów. Jej szerokość wynosi od 2 do 150 km. Stanowi największy na świecie zespół koralowców, złożony z 3 tysięcy pojedynczych raf i ponad 900 wysp

<img src="rafa.jpeg"     alt="Markdown Monster icon" height="300" style="float: left; margin-right: 1px; " />



## Slajd nr 2 - Wielki Mur Chiński

Chiński Mur w roku 2007 ogłoszono jednym z siedmiu nowych cudów świata. Jego długość oceniano wtedy na 6300 km, w późniejszych latach skorygowano na blisko 9000 km, a niektórzy naukowcy dowodzą, że mógł ciągnąć się przez nawet 22 tys. km. Początki Wielkiego Muru powstały w Okresie Walczących Królestw (około VII wieku p.n.e.). Częściowo składał się z umocnień ziemnych, a w dużej części z barier naturalnych, jak góry i woda. Miał on za zadanie chronić państwo przed najazdami hord stepowych z terenów Mongolii. 

<img src="Mur.jpg"     alt="Markdown Monster icon" height="300" style="float: left; margin-right: 1px; " />



## Slajd nr 3 - Kanion Kolorado

Kanion, będący największym przełomem rzeki na świecie, ma **446 km** długości i głębokość dochodzącą do 2133 m, a jego szerokość waha się od 800 m do 29 km. Do Wiel­kie­go Ka­nio­nu Ko­lo­ra­do (Grand Ca­ny­on of the Co­lo­ra­do) co roku przy­by­wa­ją 3 mln ludzi.

<img src="kanion.png"     alt="Markdown Monster icon" height="300" style="float: left; margin-right: 1px; " />

## Slajd nr 4 - Wodospad Iguazu

Nie­zwy­kły wo­do­spad, wi­zy­tów­ka całej Ame­ry­ki Po­łu­dnio­wej jest naj­ła­twiej do­stęp­ny od stro­ny bra­zy­lij­skiej, choć z pew­no­ścią nie wolno po­mi­nąć wspa­nia­łych wi­do­ków z ar­gen­tyń­skiej czę­ści. Z progu skal­ne­go roz­cią­ga­ją­ce­go się na ponad 3 km spa­da­ją ogrom­ne masy wody spra­wia­ją­ce wra­że­nie jakby zie­mia pod sto­pa­mi drża­ła. Wszyst­kie ta­ ra­sy są od­su­nię­te od cze­lu­ści, do któ­rych spada woda, tak aby móc objąć wzro­kiem cały ogrom wo­do­spa­dów.

<img src="wodospad.jpg"     alt="Markdown Monster icon" height="300" style="float: left; margin-right: 1px; " />

## Slajd nr 5 - Puszcza Białowieska

**Puszcza Białowieska to jeden z najcenniejszych kompleksów leśnych Europy.** Prowadzone od lat badania wykazują bytowanie w tych lasach i ich okolicach **wielu cennych gatunków fauny i flory**. Tu także żyje **największa wolna populacja żubra europejskiego**. **Dominujące** gatunki puszczańskich drzew to **świerk i sosna** (często występują również olsze, dęby i brzozy). Faunę (poza wymienionymi wcześniej żubrami) reprezentują także **rysie, wilki, bobry, nietoperze i mniejsze ssaki**. Liczną grupę stanowią również **ptaki** (kilkadziesiąt gatunków znajduje się pod ochroną).

<img src="puszcza.jpg"     alt="Markdown Monster icon" height="300" style="float: left; margin-right: 1px; " />